class Padre extends Abuelo{ //extends es para usar una herencia
    constructor() {
        super();//para que se ejecute el constructor
     this.nombre = "Brandon";//this. es para darle valor de la clase
     this.apellidoPaterno="Duran";
     this.apellidoMaterno="Rocha";
    }
    saludar(){//metodo publico
        alert("Hola Mundo");
    }
    static sumar(n1,n2){//metodo estatico
        return n1+n2
    }
    get obtenerNombre(){
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno
    }
    set cambiarNombre(nombre){
       this.nombre= nombre;
    }
}
/*Metodo publico: Datos son acc
* Metodo estatico */